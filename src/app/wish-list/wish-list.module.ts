import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToggleComponent } from './feature/toggle/toggle.component';
import { OverviewComponent } from './feature/overview/overview.component';
import { BadgeComponent } from './feature/badge/badge.component';
import { WishlistAPI } from './application/wish-list-api.service';
import { AddComponent } from './feature/add/add.component';
import { WishlistService } from './infrastructure/wishlist.service';
import { ItemComponent } from './ui/item/item.component';



@NgModule({
  declarations: [
    ToggleComponent,
    OverviewComponent,
    BadgeComponent,
    AddComponent,
    ItemComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ToggleComponent,
    OverviewComponent,
    BadgeComponent,
    AddComponent,
    ItemComponent
  ],
  providers: [
    WishlistAPI,
    WishlistService
  ]
})
export class WishListModule { }
