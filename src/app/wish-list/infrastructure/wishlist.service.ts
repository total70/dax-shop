import { Injectable } from "@angular/core";
import { Product } from "src/app/product/domain/entity";
import { ProductDTO } from "src/app/product/domain/interface";
import { WishlistItem } from "../domain/entity";
import { WishlistDTO } from "../domain/interface";

@Injectable()
export class WishlistService {

    private resetLocalStorage(): void {
        localStorage.removeItem('wishes');
    }

    public loadLocalStorage(): WishlistItem<Product>[] {
        const items = localStorage.getItem("wishes")
        if (items != null) {
            try {
                const wishes: WishlistDTO<ProductDTO>[] = JSON.parse(items);
                return wishes.map((wish) => {
                    const product = new Product(wish.item);
                    const wishlistItem = new WishlistItem<Product>(product);
                    wishlistItem.setQuantity(wish.quantity);
                    return wishlistItem
                })
            } catch (e) {
                console.error(e);
                this.resetLocalStorage();
            }
        }
        return []
    }

    public saveLocalStorage(wishes: WishlistItem<Product>[]) {
        const dtoList = wishes.map((wish) => {
            const wishItem: WishlistDTO<Product> = {
                quantity: wish.quantity,
                item: wish.item
            }
            return wishItem
        })
        localStorage.setItem("wishes", JSON.stringify(dtoList));
    }
}