import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable, take } from "rxjs";
import { Product } from "src/app/product/domain/entity";
import { WishlistItem } from "../domain/entity";
import { WishlistService } from "../infrastructure/wishlist.service";

@Injectable()
export class WishlistAPI {
    private sidePanelOpen: BehaviorSubject<boolean>;
    public sidePanelOpen$: Observable<boolean>;
    private items: BehaviorSubject<WishlistItem<Product>[]>
    public items$: Observable<WishlistItem<Product>[]>

    constructor(private wishlistService: WishlistService) {
        this.sidePanelOpen = new BehaviorSubject<boolean>(false);
        this.sidePanelOpen$ = this.sidePanelOpen.asObservable();
        this.items = new BehaviorSubject<WishlistItem<Product>[]>([]);
        this.items$ = this.items.asObservable();
    }

    toggleSidePanel(): void {
        this.sidePanelOpen.pipe(take(1)).subscribe((toggle) => this.sidePanelOpen.next(!toggle))
    }

    addItem(item: Product): void {
        this.items.pipe(take(1)).subscribe((items) => {
            const itemAlreadyExists = items.find((innerItem) => innerItem.item.getID() == item.getID());
            if (itemAlreadyExists) {
                itemAlreadyExists.quantity++;
            } else {
                items.push(new WishlistItem(item));
            }
            this.wishlistService.saveLocalStorage(items);
            this.items.next(items);
        })
    }

    increaseQuantity(id: number): void {
        this.items.pipe(take(1)).subscribe((items) => {
            items.map((wish) => {
                if (wish.item.getID() == id) {
                    wish.increaseQuantity()
                }
            })
            this.wishlistService.saveLocalStorage(items);
            this.items.next(items);
        })
    }

    removeWishItem(id: number): void {
        this.items.pipe(take(1)).subscribe((items) => {
            const newWishList = items.filter((wish) => wish.item.getID() !== id)
            this.wishlistService.saveLocalStorage(newWishList);
            this.items.next(newWishList);
        })
    }

    decreaseQuantity(id: number): void {
        let removeFromList = false;
        this.items.pipe(take(1)).subscribe((items) => {
            items.map((wish) => {
                if (wish.item.getID() === id) {
                    wish.decreaseQuantity()
                    if (wish.quantity === 0 ) {
                        removeFromList = true;
                    }
                }
            });
            if (removeFromList) {
                return this.removeWishItem(id);
            }
            this.wishlistService.saveLocalStorage(items);
            this.items.next(items);
        })
    }

    loadFromStorage(): void {
        const wishes = this.wishlistService.loadLocalStorage();
        this.items.next(wishes);
    }
}