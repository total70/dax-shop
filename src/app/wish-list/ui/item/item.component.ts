import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/product/domain/entity';
import { WishlistAPI } from '../../application/wish-list-api.service';
import { WishlistItem } from '../../domain/entity';

@Component({
  selector: 'wish-list-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input()
  wishItem!: WishlistItem<Product>;

  constructor(public wishlistAPI: WishlistAPI) { }

  ngOnInit(): void {
  }

}
