export interface WishlistDTO<T> {
    quantity: number
    item: T
}