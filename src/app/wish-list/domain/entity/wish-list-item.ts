export class WishlistItem<T> {
    public quantity: number;
    public item: T;

    constructor(item: T) {
        this.quantity = 1;
        this.item = item;
    }

    public increaseQuantity(): void {
        this.quantity++;
    }

    public decreaseQuantity(): void {
        this.quantity--;
    }

    public setQuantity(quantity: number) {
        this.quantity = quantity;
    }
}