import { Component, OnInit } from '@angular/core';
import { WishlistAPI } from '../../application/wish-list-api.service';

@Component({
  selector: 'wish-list-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss']
})
export class ToggleComponent implements OnInit {

  constructor(public wishlistAPI: WishlistAPI) { }

  ngOnInit(): void {
  }

}
