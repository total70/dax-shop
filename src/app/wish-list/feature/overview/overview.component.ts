import { Component, OnInit } from '@angular/core';
import { WishlistAPI } from '../../application/wish-list-api.service';

@Component({
  selector: 'wish-list-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  constructor(public wishlistAPI: WishlistAPI) { }

  ngOnInit(): void {
  }

}
