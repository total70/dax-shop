import { Component, OnInit } from '@angular/core';
import { WishlistAPI } from '../../application/wish-list-api.service';

@Component({
  selector: 'wish-list-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss']
})
export class BadgeComponent implements OnInit {

  constructor(public wishlistAPI: WishlistAPI) { }

  ngOnInit(): void {
  }

}
