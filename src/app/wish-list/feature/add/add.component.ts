import { Component, Input, OnInit } from '@angular/core';
import { Product } from 'src/app/product/domain/entity';
import { WishlistAPI } from '../../application/wish-list-api.service';

@Component({
  selector: 'wish-list-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  @Input()
  product!: Product;

  constructor(public wishlistAPI: WishlistAPI) { }

  ngOnInit(): void {
  }

}
