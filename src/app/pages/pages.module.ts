import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ProductModule } from '../product/product.module';
import { HeaderComponent } from './ui/header/header.component';
import { WishListModule } from '../wish-list/wish-list.module';



@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    ProductModule,
    WishListModule
  ],
  exports: [
    HomeComponent
  ]
})
export class PagesModule { }
