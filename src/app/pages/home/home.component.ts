import { Component, OnInit } from '@angular/core';
import { WishlistAPI } from 'src/app/wish-list/application/wish-list-api.service';

@Component({
  selector: 'pages-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public wishlistAPI: WishlistAPI) {
    this.wishlistAPI.loadFromStorage();
  }

  ngOnInit(): void {
  }

}
