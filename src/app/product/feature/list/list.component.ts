import { Component, OnInit } from '@angular/core';
import { ProductAPI } from '../../application/product-api.service';

@Component({
  selector: 'product-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(public productAPI: ProductAPI) {
    this.productAPI.loadProducts()
  }

  ngOnInit(): void {
  }

}
