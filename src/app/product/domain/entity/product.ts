import { ProductDTO } from "../interface";

export class Product {
    private id: number;
    public name: string;
    public image: string;
    public price: number;

    constructor(productDTO: ProductDTO) {
        this.id = productDTO.id;
        this.name = productDTO.name;
        this.image = productDTO.image;
        this.price = productDTO.price;
    }

    public getID(): number {
        return this.id;
    }
}