import { ProductDTO } from "./product-dto";

export interface ProductRequest {
    product: ProductDTO[]
}