import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './feature/list/list.component';
import { ProductService } from './infrastructure/product.service';
import { HttpClientModule } from '@angular/common/http'
import { ProductAPI } from './application/product-api.service';
import { ItemComponent } from './feature/item/item.component';
import { WishListModule } from '../wish-list/wish-list.module';

@NgModule({
  declarations: [
    ListComponent,
    ItemComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    WishListModule
  ],
  exports: [
    ListComponent
  ],
  providers: [
    ProductService,
    ProductAPI
  ]
})
export class ProductModule { }
