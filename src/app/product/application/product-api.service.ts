import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { Product } from "../domain/entity";
import { ProductService } from "../infrastructure/product.service";

@Injectable()
export class ProductAPI {
    private products: BehaviorSubject<Product[]>;
    public products$: Observable<Product[]>;

    constructor(private productService: ProductService) {
        this.products  = new BehaviorSubject<Product[]>([]);
        this.products$ = this.products.asObservable();
    }

    loadProducts(): void {
        this.productService.getProducts().subscribe((products) => this.products.next(products));
    }
}