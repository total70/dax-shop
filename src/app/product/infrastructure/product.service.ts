import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { Product } from "../domain/entity";
import { ProductDTO, ProductRequest } from "../domain/interface";

@Injectable()
export class ProductService {
    constructor(private http: HttpClient) {}

    public getProducts(): Observable<Product[]> {
        return this.http.get<ProductRequest>('/assets/products.json').pipe(map(
            (response: ProductRequest) => {
                return response.product.map((product: ProductDTO) => new Product(product))
            }
        ))
    }
}