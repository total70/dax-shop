# Dax shop

Build a basic Product Listing Page, showing some fake products originating from a JSON file of your own creation.

Add a “wishlist” widget that allows adding/removing products on the wishlist. Wishlist items should have a quantity that can be changed. Wishlist items should persist between browser sessions.

The page should have a header with a company logo and a “favorites” icon. The favorites icon should have a badge showing the number of items currently on the wishlist, much like a shopping cart on a webshop that is showing the number of items in the cart.

The wishlist itself should be presented in a “side panel” which can be opened by clicking the “favorites” icon in the header. The side panel should overlay the Product Listing Page.

And Finally; Think outside the box. This is an invitation to showcase your craftsmanship and creativity, to show what’s important to you, and we like to be technically surprised.